<!DOCTYPE html>

    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<title> Responsive Login and Signup Form </title>-->

        <!-- CSS -->
        <link rel="stylesheet" href="css/style.css">
        <link href='taa.css' rel='stylesheet'>
      </head>
        <body>
<header>
  <h1>My Profile</h1>
  <p>Selamat Datang di Profile Saya</p>
</header>
<main>
  <article>
    <center>
    <img alt="Photo Profile" src="sasqiaa.jpeg" height="140" width="100" />
  </center>
    <h2>SASQIA AULIA NUR AINI</h2>
    <h3>Berikut Biodata Saya :</h3>
    <p>Hii! Perkenalkan nama saya Sasqia Aulia Nur Aini<br>
    TTL : Purworejo, 13 Oktober 2003<br>
    NIM : 2100018456<br>
    Kelas : I<br>
    Prodi : Informatika<br>
    Universitas : Universitas Ahmad Dahlan<br>
    Hobi : Membaca, Berenang, dan Menonton Film</p>      
  </article>
  <form action="" method="post">
  <div class="row">
    <label>Nama</label>
    <input type="text" name="nama" value="<?=isset($_POST['nama']) ? $_POST['nama'] : ''?>"/>
  </div>
  <div class="row">
    <label>Email</label>
    <input type="text" name="email" value="<?=isset($_POST['email']) ? $_POST['email'] : ''?>"/>
  </div>
  <div class="row">
    <label>Lokasi</label>
    <select name="area">
      <?php $options = array('Jakarta', 'Semarang', 'Surakarta', 'Yogyakarta', 'Surabaya');
      foreach ($options as $area) {
        $selected = @$_POST['area'] == $area ? ' selected="selected"' : '';
        echo '<option value="' . $area . '"' . $selected . '>' . $area . '</option>';
      }?>
    </select>
  </div>
  <div class="row">
    <label>Jenis Kelamin</label>
    <div class="options">
      <?php
      $jenis_kelamin = array('L' => 'Laki Laki', 'P' => 'Perempuan');
      foreach ($jenis_kelamin as $kode => $detail) {
        $checked = @$_POST['jenis_kelamin'] == $kode ? ' checked="checked"' : '';
        echo '<label class="radio">
            <input name="jenis_kelamin" type="radio" value="' . $kode . '"' . $checked . '>' . $detail . '</option>
          </label>';
      }
      ?>
    </div>
  </div>
  <div class="row">
    <label>Hobi</label>
    <input type="text" name="hobi" value="<?=isset($_POST['hobi']) ? $_POST['hobi'] : ''?>"/>
  </div>
  <div class="row">
    <input type="submit" name="submit" value="Simpan"/>
  </div>
</form>
<?php
if (isset($_POST['submit'])) {
  echo '<h1>Hasil Input</h1>';
  echo '<ul>';
  echo '<li>Nama: ' . $_POST['nama'] . '</li>';
  echo '<li>Email: ' . $_POST['email'] . '</li>';
  echo '<li>Lokasi: ' . $_POST['area'] . '</li>';
  echo '<li>Jenis Kelamin: ' . (isset($_POST['jenis_kelamin']) ? $jenis_kelamin[$_POST['jenis_kelamin']] : '-') . '</li>';
  echo '<li>Hobi: ' . $_POST['hobi'] . '</li>';
}?>
</main>
<footer>
  <p>Copyright &copy; <script>document.write(new Date().getFullYear());</script>, Sasqia</p>
</footer>
</body>
</html>