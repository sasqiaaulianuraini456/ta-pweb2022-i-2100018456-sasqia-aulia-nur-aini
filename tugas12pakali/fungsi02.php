<?php

//fungsi ini dengan return value, &  parameter
function cetak_prima($awal,$akhir){
    for ($i=$awal; $i <$akhir; $i++) { 
        if ($i%7==0) {
            echo "$i,";
        }
    }
}
//pemanggilan fungsi
$a = 0;
$b = 200;
echo "<b>Bilangan PRIMA dari $a sampai $b, adalah : </b><br>";
cetak_prima($a,$b);
?>