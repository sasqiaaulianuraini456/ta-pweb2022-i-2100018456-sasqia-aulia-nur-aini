<?php
//Fungsi dengan return value dan parameter
function luas_persegi($sisi){
    return $sisi*$sisi;
}

//pemanggil fungsi
$sisi = 15;
echo "Luas persegi dengan panjang sisi $sisi = ";
echo luas_persegi($sisi);
?>