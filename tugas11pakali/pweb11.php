<!-- Form input type text and password -->
<!DOCTYPE html>
<html>
<head>
    <title><h3>Form input type TEXT dan PASSWORD</h3></title>
</head>
<body>
    <form action="" method="post" name="input">
        Universitas Pilihan Daftar Ujian Mandiri Masuk Perguruan Tinggi Negeri <br>
        1. <input type="text" name="prodi1"><br>
        2. <input type="text" name="prodi2"><br>
        <input type="submit" name="Input" value="Submit">
    </form>
</body>
</html>

<?php
if (isset($_POST['input'])){
    $prodi1 = $_POST['prodi1'];
    $prodi2 = $_POST['prodi2'];

    echo "<b>  Universitas Pilihan Daftar Ujian Mandiri Masuk Perguruan Tinggi Negeri  : </b><br>";
    echo "1) $prodi1 <br>";
    echo "2) $prodi2 <br>";
}
?>


<!-- Form input type radio -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type RADIO</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="pilih">
        <h2> Katalog Dior Parfum Indonesia</h2>
        <input type="radio" name="parfum" value="MISSD"> Miss Dior Eau de Parfum Roller Pearl <br/>
        <input type="radio" name="parfum" value="SAUVAGE"> Sauvage Eau De Parfum <br/>
        <input type="radio" name="parfum" value="MISSDH"> Miss Dior Hair Mist<br/>
        <input type="radio" name="parfum" value="HOMME"> Homme Eau de Toilette <br/>
        <input type="submit" name="Input" value="Pilih">
    </form>
</body>
</html>

<?php
if (isset($_POST['parfum'])){
    $sembako = $_POST['parfum'];
    echo "Dior Parfum Pilihan Anda : <b><font color='blue'> $parfum </font><b>";
}
?>


<!-- Form input type checkbox -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type checkbox</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="pilih">
        <h2> Provinsi Tempat Tinggal Anda : </h2>
        <input type="checkbox" name="prov1" value="JAWAT"> Jawa Tengah <br/>
        <input type="checkbox" name="prov2" value="YOG"> Yogyakarta <br/>
        <input type="checkbox" name="prov3" value="JATIM"> Jawa Timur <br/>
        <input type="checkbox" name="prov4" value="JABAR"> Jawa Barat <br/>
        <input type="checkbox" name="prov5" value="DKI"> DKI Jakarta <br/>
        <input type="submit" name="Input" value="Pilih">
    </form>
</body>
</html>

<?php
    echo "Masukkan Provinsi Tempat Tinggal Anda : <br>";
    if (isset($_POST['prov1'])){
        echo "- " . $_POST['prov1'] . "<br>";
    }
    if (isset($_POST['prov2'])){
        echo "- " . $_POST['prov2'] . "<br>";
    }
    if (isset($_POST['prov3'])){
        echo "- " . $_POST['prov3'] . "<br>";
    }
    if (isset($_POST['prov4'])){
        echo "- " . $_POST['prov4'] . "<br>";
    }
    if (isset($_POST['prov5'])){
        echo "- " . $_POST['prov5'] . "<br>";
    }
?>


<!-- Form input type combobox -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type combobox</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="pilih">
        <h2> Pilih buah kesukaan anda</h2>
        <select name="buah">
            <option value="Durian">Durian</option>
            <option value="Semangka">Semangka</option>
            <option value="Rambutan">Rambutan</option>
            <option value="Nanas">Nanas</option>
        </select>
        <input type="submit" name="Pilih" value="Pilih">
</body>
</html>

<?php
    if (isset($_POST['buah'])){
        $buah = $_POST['buah'];
        echo "Buah favoritmu adalah : $buah <br>";
    }
?>


<!-- Form input type text area -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type text area</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="tulis">
        <h2> Biodata Saya </h2>
        <textarea name="biodata" cols="40" rows="6"></textarea><br>
        <input type="submit" name="tulis" value="Input Biodata">
</body>
</html>

<?php
    if (isset($_POST['tulis'])){
        $biodata = nl2br($_POST['biodata']);
        echo "Berikut adalah biodata saya : <br>";
        echo "$biodata";
    }
?>


<!-- Form input type Form Validation -->
<!DOCTYPE html>
<html>
<head>
    <title> Form Validation </title>
    <style>.error {color: #FF0000;}</style>
</head>
<body>

<?php
$namaErr = "";
$nama = "";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    if (empty($_POST["nama"])){
        $namaErr = "Nama harus diisi!";
    } else{
        $nama = test_input($_POST["nama"]);
        if (!preg_match("/^[a-zA-Z]*$/", $nama)){
            $namaErr = "Only letters and white space allowed";
        }
    }
}

function test_input($data){
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

    <h2> Validasi Form </h2>
    <p><span class="error">* Masukkan nama.</span></p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        Nama : <input type="text" name="nama" value="<?php echo $nama;?>">
        <span class="error">* <?php echo $namaErr;?></span>
        <br><br>
        <input type="submit" name="submit" value="Submit">
    </form>

<?php
echo "<h2> Hasil inputan : </h2>";
echo $nama;
echo "<br>"
?>

</body>
</html>