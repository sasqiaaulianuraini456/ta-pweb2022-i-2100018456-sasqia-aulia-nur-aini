<!-- Form input type checkbox -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type checkbox</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="pilih">
        <h2> Provinsi Tempat Tinggal Anda : </h2>
        <input type="checkbox" name="prov1" value="JATENG"> Jawa Tengah <br/>
        <input type="checkbox" name="prov2" value="YOGYAKARTA"> Yogyakarta <br/>
        <input type="checkbox" name="prov3" value="JATIM"> Jawa Timur <br/>
        <input type="checkbox" name="prov4" value="JABAR"> Jawa Barat <br/>
        <input type="checkbox" name="prov5" value="DKI"> DKI Jakarta <br/>
        <input type="submit" name="Input" value="Pilih">
    </form>
</body>
</html>

<?php
    echo "Masukkan Provinsi Tempat Tinggal Anda : <br>";
    if (isset($_POST['prov1'])){
        echo "- " . $_POST['prov1'] . "<br>";
    }
    if (isset($_POST['prov2'])){
        echo "- " . $_POST['prov2'] . "<br>";
    }
    if (isset($_POST['prov3'])){
        echo "- " . $_POST['prov3'] . "<br>";
    }
    if (isset($_POST['prov4'])){
        echo "- " . $_POST['prov4'] . "<br>";
    }
    if (isset($_POST['prov5'])){
        echo "- " . $_POST['prov5'] . "<br>";
    }
?>
