<?php
$gaji = 1000000;
$pajak = 0.1;
$kas = 35000;
$thp = $gaji - ($gaji*$pajak);
$hasil = $thp - $kas;
echo "Gaji sebelum pajak = Rp. $gaji <br>";
echo "Gaji yang dibawa pulang = Rp. $thp <br>";
echo "Gaji bersih = Rp. $hasil";
echo "<hr><hr>";

$a = 5;
$b = 4;

echo "$a == $b : ". ($a == $b);
echo "<br>$a != $b : ". ($a != $b);
echo "<br>$a > $b : ". ($a > $b);
echo "<br>$a < $b : ". ($a < $b);
echo "<br>($a == $b) && ($a > &b) : ".(($a != $b) && ($a > $b));
echo "<br>($a == $b) || ($a > &b) : ".(($a != $b) || ($a > $b));
echo "<br>$a + 2 > $b + 7 : ". (($a + 2) > ($b + 7));
echo "<br>$a + 2 < $b + 7 : ". (($a + 2) < ($b + 7));
?>